//
//  BasicDemoApp.swift
//  BasicDemo
//
//  Created by Carlos Ramos on 18/12/23.
//

import SwiftUI

@main
struct BasicDemoApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
