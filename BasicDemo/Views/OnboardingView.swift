//
//  OnboardingView.swift
//  BasicDemo
//
//  Created by Carlos Ramos on 18/12/23.
//

import SwiftUI

struct OnboardingView: View {
    @StateObject var viewModel = OnboardingViewModel()
    @State private var hasCompletedOnboarding = false
    @State private var navigateToLogin = false

    var body: some View {
        NavigationStack {
            TabView(selection: $viewModel.currentIndex) {
                ForEach(0..<3) { index in
                    OnboardingSlide(index: index, hasCompletedOnboarding: $hasCompletedOnboarding, customAction: viewModel.goToNextSlide)
                }
            }
            .tabViewStyle(PageTabViewStyle())
            .onChange(of: hasCompletedOnboarding) { oldValue, newValue in
                if newValue {
                    navigateToLogin = true
                }
            }
            .navigationDestination(isPresented: $navigateToLogin) {
                LoginView()
            }
        }
    }
}


struct OnboardingSlide: View {
    let index: Int
    @Binding var hasCompletedOnboarding: Bool
    var customAction: () -> Void?

    var body: some View {
        VStack {
            Text("Slide \(index + 1)")
            Button("Next") {
                if index == 2 {
                    hasCompletedOnboarding.toggle()
                } else {
                    customAction()
                }
            }
        }
    }
}


