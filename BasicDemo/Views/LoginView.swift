//
//  LoginView.swift
//  BasicDemo
//
//  Created by Carlos Ramos on 18/12/23.
//

import SwiftUI

struct LoginView: View {
    @StateObject var viewModel = LoginViewModel()
    @State private var navigateToHome: Bool = false

    var body: some View {
        VStack {
            TextField("Username", text: $viewModel.username)
            SecureField("Password", text: $viewModel.password)
            Button("Login") {
                viewModel.login()
                if viewModel.isAuthenticated {
                    navigateToHome = true
                }
            }
            /// PRESENT THE VIEW on a modal way
            .sheet(isPresented: $navigateToHome, content: {
                HomeView()
            })
            /// PUSH THE VIEW on the current navigation stack
//            .navigationDestination(isPresented: $navigateToHome) {
//                HomeView()
//            }
        }
    }
}
