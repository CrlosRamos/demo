//
//  ContentView.swift
//  BasicDemo
//
//  Created by Carlos Ramos on 18/12/23.
//

import SwiftUI

struct ContentView: View {

    var body: some View {
        OnboardingView()
    }
}


#Preview {
    ContentView()
}
