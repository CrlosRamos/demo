//
//  HomeView.swift
//  BasicDemo
//
//  Created by Carlos Ramos on 18/12/23.
//

import SwiftUI

struct HomeView: View {
    var body: some View {
        Text("Welcome to the Home View")
    }
}
