//
//  OnboardingViewModel.swift.swift
//  BasicDemo
//
//  Created by Carlos Ramos on 18/12/23.
//

import Foundation

class OnboardingViewModel: ObservableObject {
    @Published var currentIndex: Int = 0

    func goToNextSlide() {
        currentIndex = min(currentIndex + 1, 2)
    }
}
