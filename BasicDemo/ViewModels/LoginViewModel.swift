//
//  LoginViewModel.swift
//  BasicDemo
//
//  Created by Carlos Ramos on 18/12/23.
//

import Foundation

class LoginViewModel: ObservableObject {
    @Published var username: String = ""
    @Published var password: String = ""
    @Published var isAuthenticated: Bool = false

    func login() {
        isAuthenticated = true
    }
}
