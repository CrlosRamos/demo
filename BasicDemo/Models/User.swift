//
//  User.swift
//  BasicDemo
//
//  Created by Carlos Ramos on 18/12/23.
//

import Foundation

struct User: Codable {
    var id: String
    var name: String
}
